﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mysql2SQLite
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        MySql _mysql = new MySql();
        SQLite _sqlite = new SQLite();

        private void btnConnect_Click(object sender, EventArgs e)
        {
            //TextBox에 입력된 데이터베이스 연결
            if(_mysql.Connection(txtServer.Text, txtUserID.Text, txtPWS.Text, txtDB.Text))
            {
                Log("MySql Connection Successed.");

                //현재 데이터베이스에 존재하는 테이블 목록 가져오기
                DataSet ds = _mysql.SendQuery("show TABLES");
                cbTable.Items.Clear();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string table = dr[$"tables_in_{txtDB.Text}"].ToString();
                    cbTable.Items.Add(table);
                }
                if (cbTable.Items.Count > 0) cbTable.SelectedIndex = 0;
            } else
            {
                Log("MySql Connection Failed.");
            }
        }

        private void cbMySqlTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            //선택된 테이블의 모든 레코드 읽기
            DataSet ds = _mysql.SendQuery("SELECT * FROM " + cbTable.Text);
            dgvTable.DataSource = ds.Tables[0];

            //선택된 테이블 생성 쿼리 가져오기
            DataSet dst = _mysql.SendQuery($"Show CREATE TABLE {cbTable.Text}");
            foreach (DataRow dr in dst.Tables[0].Rows)
            {
                txtTableQuery.Text = dr["CREATE TABLE"].ToString();
            }
        }

        private void btnMysql2Sqlite_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //생성할 SQLite 파일 경로 가져오기
                _sqlite.DBPath = sfd.FileName;
                if (cbTable.Items.Count > 0)
                {
                    //현재 데이터베이스에 존재하는 테이블만큼 반복
                    for (int i = 0; i < cbTable.Items.Count; i++) //Table Create & Data Move
                    {
                        string table = cbTable.Items[i].ToString();

                        DataSet dst = _mysql.SendQuery($"Show CREATE TABLE {table}");
                        foreach (DataRow dr in dst.Tables[0].Rows)
                        {
                            //테이블 생성 쿼리 이용하여 SQLite 테이블 생성
                            //SQLite의 데이터 타입은 5가지 유형(NULL, INTEGER, REAL, TEXT, BLOB)이 있지만,
                            //다른 DB에서 사용하는 데이터 이름 그대로 사용해도 문제가 없음
                            string tableQuery = dr["CREATE TABLE"].ToString();
                            int index = tableQuery.IndexOf("ENGINE");
                            tableQuery = tableQuery.Substring(0, index);

                            if (_sqlite.CreateTable(tableQuery))
                            {
                                Log($"'{table}' Table Creation Successed.");
                            }
                            else
                            {
                                Log($"'{table}' Table Already Exist.");
                            }
                            
                            //테이블 생성 쿼리에서 컬럼데이터만 가져오기
                            string[] column = tableQuery.Split('\n');
                            column = column.Where(x => x != column[column.Length - 1]).ToArray();
                            column = column.Where(x => x != column[0]).ToArray();

                            //현재 테이블 row 가져와서 SQLite에 입력하기
                            DataSet ds = _mysql.SendQuery("SELECT * FROM " + table);
                            dgvTable.DataSource = ds.Tables[0];

                            if (dgvTable.Rows.Count > 0)
                            {
                                for (int j = 0; j < dgvTable.Rows.Count; j++)
                                {
                                    string value = ""; //테이블 row 입력 쿼리
                                    for (int k = 0; k < dgvTable.ColumnCount; k++)
                                    {
                                        //컬럼별 데이터 타입 가져오기
                                        string[] columnData = column[k].Split(' ');
                                        if (columnData[3].Contains("int")) //컬럼이 int인 경우
                                        {
                                            value = value + $"{dgvTable.Rows[j].Cells[k].Value}, ";
                                        }
                                        else if (columnData[3].Contains("date")) //컬럼이 date인 경우
                                        {
                                            DateTime dt = DateTime.Parse(dgvTable.Rows[j].Cells[k].Value.ToString());
                                            string date = dt.ToString("yyyy-MM-dd");
                                            value = value + $"'{date}', ";
                                        }
                                        else //컬럼이 다른 타입(text)인 경우
                                        {
                                            value = value + $"'{dgvTable.Rows[j].Cells[k].Value}', ";
                                        }
                                    }
                                    value = value.Substring(0, value.Length - 2);
                                    //row 입력하기
                                    _sqlite.Insert(table, value);
                                }
                                Log($"'{table}' Data Moved from MySql.");
                            }
                        }
                    }
                    Log("Finished.");
                } else
                {
                    Log("Table is not existed.");
                }
            }
        }

        public void Log(string log)
        {
            txtLog.AppendText(log + "\r\n");
        }
    }
}
