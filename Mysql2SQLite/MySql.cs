﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Mysql2SQLite
{
    public class MySql
    {
        MySqlConnection conn;
        MySqlDataAdapter adpt;
        MySqlCommand cmd;

        public bool Connection(string server, string id, string pwd, string db)
        {
            try
            {
                conn = new MySqlConnection($"server={server};uid={id};pwd={pwd};database={db};pooling=false;allow user variables=true");
                conn.Open();
                conn.Close();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }
        }

        public DataSet SendQuery(string sql)
        {
            try
            {
                DataSet ds = new DataSet();
                adpt = new MySqlDataAdapter(sql, conn);
                adpt.Fill(ds);
                if (ds.Tables.Count > 0) return ds;
                else return null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                throw;
            }
        }
    }
}
