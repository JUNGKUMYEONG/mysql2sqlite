﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Mysql2SQLite
{
    public class SQLite
    {
        public string DBPath;
        public string dataSource;

        public bool CreateTable(string sql)
        {
            try
            {
                dataSource = $@"Data Source={DBPath};Version=3";
                if (!System.IO.File.Exists(DBPath))
                {
                    SQLiteConnection.CreateFile(DBPath);
                }

                SQLiteConnection sqliteConn = new SQLiteConnection(dataSource);
                sqliteConn.Open();
                SQLiteCommand cmd = new SQLiteCommand(sql, sqliteConn);
                cmd.ExecuteNonQuery();
                sqliteConn.Close();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }
        }

        public void Insert(string table, string value)
        {
            try
            {
                dataSource = $@"Data Source={DBPath}";
                using (SQLiteConnection conn = new SQLiteConnection(dataSource))
                {
                    conn.Open();
                    string sql = $"INSERT INTO {table} VALUES ({value})";
                    SQLiteCommand cmd = new SQLiteCommand(sql, conn);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                throw;
            }
        }
    }
}
